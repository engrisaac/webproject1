from django.db import models

class Box(models.Model):
    box_title = models.CharField(max_length = 30)
    box_text = models.TextField()
    

    def __str__(self):
        return "title : {} => text : {}".format(self.title, self.text)