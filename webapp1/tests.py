from django.test import TestCase

class TestPage(TestCase):
    def test_base_page_works(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code , 200)
        self.assertTemplateUsed(response, 'base.html')
        
    def test_index_page_works(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code , 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertContains(response , 'list_text', html =True)

    def test_create_page_works(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code , 200)
        self.assertTemplateUsed(response, 'create.html')
        self.assertContains(response , 'create_text' , html =True)

               
    def test_box_delete_page_works(self):
        response = self.client.get("/")
        self.assertEqual(response.status_code , 200)
        self.assertTemplateUsed(response, 'box_delete.html')
        self.assertContains(response , 'delete_text', html =True)

