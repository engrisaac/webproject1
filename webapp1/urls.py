from django.urls import path
from . import views


urlpatterns = [
    
    path('' , views.index , name = 'list_text'),
    path('new/' , views.create , name = 'create_text' ),
    path('update/<int:id>/' , views.update , name = 'update_text'),
     path('delete/<int:id>/' , views.delete , name = 'delete_text'),
]
