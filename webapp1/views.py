from django.shortcuts import render, redirect
from .models import Box
from .forms import Boxform

def index(request):

    box = Box.objects.all()

    context = {'box' : box}

    return render(request, 'webapp1/index.html', context)


def  create(request):
    if request.method == 'POST':
       form = Boxform(request.POST or None)
       if form.is_valid():
          new_form = Box(box_title = request.POST['box_title'], box_text = request.POST['box_text'])
          new_form.save()
          return redirect("list_text")

    else:
        form = Boxform()    

    context = {'form' : form}

    return render(request , 'webapp1/create.html', context)



def  update(request , id):
    box = Box.objects.get(id=id)
    form = Boxform(request.POST or None, instance = box)

    if form.is_valid():
        new_form.save()
    context = {'form': form, 'box': box }

    return render(request , 'webapp1/create.html', context) 


def  delete(request, id):
    box = Box.objects.get(id =id)
    if request.method == 'POST':
        box.delete()
        return redirect("list_text")
        
    context = {'box': box}

    return render(request , 'webapp1/box_delete.html', context)
